const path = require("path");
const fs = require("fs");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const favicon = require("serve-favicon");
const csrf = require("csurf");
const flash = require("connect-flash");
const multer = require("multer");

const errorController = require("./controllers/error");
const User = require("./models/user");

const MONGODB_URI =
  "mongodb+srv://rani1992:rani1992@cluster0.xokvi.mongodb.net/node-complete";

const app = express();
const store = new MongoDBStore({
  uri: MONGODB_URI,
  collection: "sessions",
});
const csrfProtection = csrf();

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images");
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString().replace(/:/g, "-") + file.originalname);
  },
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/png" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/jpeg"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

app.set("view engine", "ejs");
app.set("views", "views");

const adminRoutes = require("./routes/admin");
const shopRoutes = require("./routes/shop");
const authRoutes = require("./routes/auth");
const ajaxRoutes = require("./routes/ajax");
const product = require("./models/product");
const { fillAndStroke } = require("pdfkit");

// API Calls
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(
  multer({ storage: fileStorage, fileFilter: fileFilter }).single("image")
);
app.use(express.static(path.join(__dirname, "public")));
app.use("/images", express.static(path.join(__dirname, "images")));
// API Post Product
app.post("/product", async (req, res, next) => {
  try {
    const products = new product(req.body);
    console.log(req.file);
    const reuslt = await products.save();
    res.send(reuslt);
  } catch (error) {
    res.status(5000);
  }
});

// API Get Product
app.get("/allProducts", async (req, res, next) => {
  try {
    const products = await product.find();
    res.send(products);
  } catch (error) {
    res.status(5000);
  }
});

// API Get Product By Id
app.get("/product/:id", async (req, res, next) => {
  try {
    const oneProduct = await product.findById(req.params.id);
    res.send(oneProduct);
  } catch (error) {
    res.status(5000);
  }
});

// API Put Product By Id
app.put("/product/:id", async (req, res, next) => {
  try {
    const putProduct = await product.findById(req.params.id);
    putProduct.set(req.body);
    const result = await putProduct.save();
    res.send(result);
  } catch (error) {
    res.status(404);
    res.send({ error: "Product doesn't exist!" });
  }
});
// API Delete Product
app.delete("/products/:id", async (req, res, next) => {
  try {
    await product.deleteOne({ _id: req.params.id });
    res.status(204).send();
  } catch (error) {
    res.status(404);
    res.send({ error: "Product doesn't exist!" });
  }
});

app.use(favicon(path.join(__dirname + "/public/images/favicon.ico")));
app.use(
  session({
    secret: "my secret",
    resave: false,
    saveUninitialized: false,
    store: store,
  })
);
app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {
  if (!req.session.user) {
    return next();
  }
  User.findById(req.session.user._id)
    .then((user) => {
      req.user = user;
      next();
    })
    .catch((err) => console.log(err));
});

app.use((req, res, next) => {
  res.locals.isAuthenticated = req.session.isLoggedIn;
  res.locals.csrfToken = req.csrfToken();
  next();
});

app.use("/admin", adminRoutes);
app.use(ajaxRoutes);
app.use(shopRoutes);
app.use(authRoutes);

app.use(errorController.get404);

mongoose
  .connect(MONGODB_URI)
  .then((result) => {
    app.listen(3000);
  })
  .catch((err) => {
    console.log(err);
  });
