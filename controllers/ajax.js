const { validationResult } = require("express-validator");
const Product = require("../models/product");

exports.getProducts = (req, res, next) => {
  try {
    res.render("ajax/product-ajax", {
      pageTitle: "Ajax Product",
      path: "/product-ajax",
    });
  } catch (err) {
    console.log(err);
  }
};

exports.postAddProduct = (req, res, next) => {
  try {
    return res.status(422).render("ajax/product-ajax", {
      pageTitle: "Ajax Product",
      path: "/product-ajax",
    });
  } catch (err) {
    console.log(err);
  }
};
exports.postEditProduct = (req, res, next) => {
  try {
    return res.status(422).render("ajax/product-ajax", {
      pageTitle: "Ajax Product",
      path: "/product-ajax",
    });
  } catch (error) {
    console.log(error);
  }
};

exports.postDeleteProduct = (req, res, next) => {
  try {
    return res.status(422).render("ajax/product-ajax", {
      pageTitle: "Ajax Product",
      path: "/product-ajax",
    });
  } catch (err) {
    console.log(err);
  }
};
