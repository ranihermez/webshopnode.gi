const CACHE_NAME = "webshop-v1";
const toCache = [
  "/",
  "/js/status.js",
  "/css/auth.css",
  "/css/cart.css",
  "/css/forms.css",
  "/css/main.css",
  "/css/orders.css",
  "/css/product.css",
  "/js/main.js",
  "/images/favicon.ico",
];
self.addEventListener('install', function (event) {
  event.waitUntil(
      caches.open(CACHE_NAME)
          .then(function (cache) {
              console.log('Opened cache');
              return cache.addAll(toCache);
          })
  );
});

self.addEventListener('activate', function (event) {
  event.waitUntil(
      caches.keys().then(function (keys) {
          return Promise.all(keys.map(function (key, i) {
              if (key !== CACHE_VERSION) {
                  return caches.delete(keys[i]);
              }
          }))
      })
  )
});

self.addEventListener("fetch", function (event) {
  let online = navigator.onLine
  if (!online) {
      event.respondWith(
          caches.match(event.request).then(function (res) {
              if (res) {
                  return res;
              }
              requestBackend(event);
          })
      )
  }
});

function requestBackend(event) {
  var url = event.request.clone();
  return fetch(url).then(function (res) {
      //if not a valid response send the error
      if (!res || res.status !== 200 || res.type !== 'basic') {
          return res;
      }

      var response = res.clone();

      caches.open(CACHE_NAME).then(function (cache) {
          cache.put(event.request, response);
      });

      return res;
  })
}