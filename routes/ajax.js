const express = require("express");
const ajaxController = require("../controllers/ajax");
const isAuth = require("../middleware/is-auth");

const router = express.Router();

// /ajax/products => GET
router.get("/product-ajax", isAuth, ajaxController.getProducts);

router.post("/product-ajax", isAuth, ajaxController.postAddProduct);

router.post("/product-ajax", isAuth, ajaxController.postDeleteProduct);

router.post("/product-ajax", isAuth, ajaxController.postEditProduct);

module.exports = router;
